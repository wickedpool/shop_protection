# INSTALL :

1. COPIER LE CONTENU DU FICHIER .txt

2. ALLER DANS SHOPIFY puis "MODIFIER LE CODE"

3. ALLER DANS "Snippets" ET "Ajouter un nouveau Snippet"

4. APPELER LE SNIPPET "oklm"

5. COLLER LE CONTENU DU FICHIER .txt DANS oklm.liquid

5. ALLER DANS "Layout" -> "theme.liquid"

6. AU DESSUS DE LA BALISE "<\/head>" : AJOUTER LA LIGNE SUIVANTE :

{% include 'oklm.liquid' %}
